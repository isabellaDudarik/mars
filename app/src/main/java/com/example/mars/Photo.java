package com.example.mars;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity
public class Photo {

    @PrimaryKey
    public int id;
    String img_src;

    public Photo() {
    }

    public Photo(int id, String img_src) {
        this.id = id;
        this.img_src = img_src;
    }
}
