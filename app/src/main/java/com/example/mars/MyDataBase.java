package com.example.mars;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Photo.class}, version = 1)
public abstract class MyDataBase extends RoomDatabase {
    public abstract PhotoDao getPhotoDao();
}
