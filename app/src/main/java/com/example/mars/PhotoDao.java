package com.example.mars;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;
@Dao
public abstract class PhotoDao {

    @Insert
    public abstract void insertAll (List<Photo> photos);


    @Query("SELECT * FROM Photo")
    public abstract Flowable<List<Photo>> selectAll();

    @Query("DELETE FROM Photo")
    public abstract void removeAll();

    public  void updateAll(List<Photo> photos){
        removeAll();
        insertAll(photos);
    }
}
