package com.example.mars;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<Photo> convert (PhotoResponse photoResponses){
        List<Photo> photos = new ArrayList<>();
        for (int i = 0; i < photoResponses.photos.size(); i++) {
            photos.add(new Photo(i + 1, photoResponses.photos.get(i).img_src));
        }
        return photos;


    }
}
