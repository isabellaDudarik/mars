package com.example.mars;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    Disposable disposable;
    CompositeDisposable disposables = new CompositeDisposable();
    MyDataBase dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
//        Picasso.get().load("http://mars.jpl.nasa.gov/msl-raw-images/proj/msl/redops/ods/surface/sol/01000/opgs/edr/fcam/FLB_486265257EDR_F0481570FHAZ00323M_.JPG").into(imageView, new Callback() {
//            @Override
//            public void onSuccess() {
//                Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onError(Exception e) {
//                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//        Glide.with(MainActivity.this).load("http://mars.jpl.nasa.gov/msl-raw-images/proj/msl/redops/ods/surface/sol/01000/opgs/edr/fcam/FLB_486265257EDR_F0481570FHAZ00323M_.JPG").into(imageView);

        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();

        getinfo();
        getData();
    }

    public void getinfo() {
        disposable = ApiService.getPhoto(1000)

                .map(new Function<PhotoResponse, Boolean>() {
                    @Override
                    public Boolean apply(PhotoResponse photoResponses) throws Exception {
                        List<Photo> photos = Converter.convert(photoResponses);
                        dataBase.getPhotoDao().updateAll(photos);
                        return true;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                               @Override
                               public void accept(Boolean aBoolean) throws Exception {
                                   /* DO NOTHING */
                               }
                           }

                        , new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }

    private void getData() {
        disposables.add(
                dataBase.getPhotoDao().selectAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<Photo>>() {
                            @Override
                            public void accept(List<Photo> photos) throws Exception {
                                for (Photo photo : photos) {
                                    Picasso.get().load(photos.get(0).img_src).into(imageView);
                                }
                            }
                        }));

    }
}

