package com.example.mars;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/";
    private static PrivateApi privateApi;
    public static final String API_KEY = "F7wgPwDF6UxwYj50oAqK4waZcmwSO2TcQOyAWRdT";

    public interface PrivateApi {
        @GET("photos")
        Observable<PhotoResponse> getPhotos(@Query("sol") int sol, @Query("api_key") String api_key);

    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }


    public static Observable<PhotoResponse> getPhoto(int sol) {
        return privateApi.getPhotos(sol, API_KEY);
    }


}
