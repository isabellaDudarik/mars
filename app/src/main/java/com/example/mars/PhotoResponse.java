package com.example.mars;

import java.util.List;

public class PhotoResponse {
    public List<Photo> photos;

    public class Photo {
        public String img_src;
    }

}
